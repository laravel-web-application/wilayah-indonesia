<?php

namespace App\Http\Controllers;

use App\Districts;
use App\Provinces;
use App\Regencies;
use App\Villages;

class CountryController extends Controller
{
    public function provinces()
    {
        $provinces = Provinces::all();
        return view('indonesia', compact('provinces'));
    }

    public function regencies()
    {
        $provinces_id = request()->get('province_id');
        $regencies = Regencies::where('province_id', '=', $provinces_id)->get();
        return response()->json($regencies);
    }

    public function districts()
    {
        $regencies_id = request()->get('regencies_id');
        $districts = Districts::where('regency_id', '=', $regencies_id)->get();
        return response()->json($districts);
    }

    public function villages()
    {
        $districts_id = request()->get('districts_id');
        $villages = Villages::where('district_id', '=', $districts_id)->get();
        return response()->json($villages);
    }
}
